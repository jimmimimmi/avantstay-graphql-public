package com.avantstay.headlines.graphql

import cats.effect._
import cats.implicits._
import io.circe.Json
import io.circe.optics.JsonPath.root
import org.http4s.HttpRoutes
import org.http4s.circe._
import org.http4s.dsl.Http4sDsl
import sangria.execution.QueryAnalysisError
import sangria.marshalling.circe._
import sangria.parser.{QueryParser, SyntaxError}

import scala.concurrent.ExecutionContext
import scala.util.control.NonFatal
import scala.util.{Failure, Success}

class RouteHandler[F[_]](newsRepo: NewsProvider)
                        (implicit F: ConcurrentEffect[F],
                         dbEC: ExecutionContext,
                         cs: ContextShift[F])
  extends Http4sDsl[F] {

  implicit val ioContextShift = IO.contextShift(dbEC)
  val route: HttpRoutes[F] = HttpRoutes.of[F] {
    case request@POST -> Root / "graphql" =>
      request.as[Json].flatMap { body =>
        val query = root.query.string.getOption(body)
        query.map(QueryParser.parse(_)) match {

          case Some(Success(ast)) =>
            for {
              news <- F.liftIO {
                IO.fromFuture(IO(QueryExecutor.execute(newsRepo, ast)))
              }.flatMap {
                Ok(_)
              }.recoverWith {
                case e: QueryAnalysisError => BadRequest(e.resolveError)
                case e => InternalServerError(e.toString)
              }
            } yield news

          case Some(Failure(error)) =>
            BadRequest(formatError(error))

          case None =>
            BadRequest(formatError("No query to execute"))
        }
      }
  }

  def formatError(error: Throwable): Json = error match {
    case syntaxError: SyntaxError =>
      Json.obj("errors" → Json.arr(
        Json.obj(
          "message" → Json.fromString(syntaxError.getMessage),
          "locations" → Json.arr(Json.obj(
            "line" → Json.fromBigInt(syntaxError.originalError.position.line),
            "column" → Json.fromBigInt(syntaxError.originalError.position.column))))))
    case NonFatal(e) =>
      formatError(e.getMessage)
    case e =>
      throw e
  }

  def formatError(message: String): Json =
    Json.obj("errors" → Json.arr(Json.obj("message" → Json.fromString(message))))
}
