package com.avantstay.headlines.graphql

import com.avantstay.headlines.db.ReadHeadlineDao

import scala.concurrent.{ExecutionContext, Future}

class NewsProvider(headlineDao: ReadHeadlineDao) {
  def getNews(implicit ec: ExecutionContext): Future[Seq[News]] =
    headlineDao.all.map(headlines => headlines.map(h => News(h.link, h.title)))
}
