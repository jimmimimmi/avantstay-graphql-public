package com.avantstay.headlines.graphql

import sangria.macros.derive._
import sangria.schema._
import scala.concurrent.ExecutionContext.Implicits.global

object SchemaDefinition {
  implicit private lazy val NewsType: ObjectType[Unit, News] = deriveObjectType[Unit, News]()

  private val QueryType = ObjectType("Query", fields[NewsProvider, Unit](
    Field("news", ListType(NewsType),
      description = Some("List of all news"),
      resolve = c ⇒ c.ctx.getNews)))

  val schema = Schema(QueryType)
}
