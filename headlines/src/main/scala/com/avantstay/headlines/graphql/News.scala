package com.avantstay.headlines.graphql

case class News(link: String, title: String)
