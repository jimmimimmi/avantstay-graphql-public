package com.avantstay.headlines.graphql

import io.circe.Json
import sangria.ast.Document
import sangria.execution._
import sangria.marshalling.circe._

import scala.concurrent.{ExecutionContext, Future}
import scala.language.postfixOps

object QueryExecutor {
  def execute(repo: NewsProvider, query: Document)(implicit ec: ExecutionContext): Future[Json] =
    Executor.execute(SchemaDefinition.schema, query, repo, exceptionHandler = exceptionHandler)

  val exceptionHandler = ExceptionHandler {
    case (_, e) => HandledException(e.getMessage)
  }
}
