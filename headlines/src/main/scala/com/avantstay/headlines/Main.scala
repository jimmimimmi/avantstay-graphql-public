package com.avantstay.headlines

import cats.effect.{ExitCode, IO, IOApp}
import cats.implicits._
import com.avantstay.headlines.crawling.HeadlinesCrawler
import com.avantstay.headlines.db.Initializer
import com.avantstay.headlines.http.HeadlinesServer

import scala.concurrent.ExecutionContext.Implicits._


object Main extends IOApp {
  def run(args: List[String]) = {
    Initializer.init

    val crawlingStream = HeadlinesCrawler.stream[IO]
    val httpServerSteam = HeadlinesServer.stream[IO]

    httpServerSteam.concurrently(crawlingStream).compile.drain.as(ExitCode.Success)
  }
}