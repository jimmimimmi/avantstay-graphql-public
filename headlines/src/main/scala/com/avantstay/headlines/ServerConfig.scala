package com.avantstay.headlines

import com.typesafe.config.ConfigFactory

import scala.concurrent.duration.FiniteDuration

object ServerConfig {
  private def toFiniteDuration(d: java.time.Duration): FiniteDuration =
    scala.concurrent.duration.Duration.fromNanos(d.toNanos)

  private val config = ConfigFactory.load()

  object Flyway {
    val schema = config.getString("db.schema")
    val url = config.getString("db.url")
    val username = config.getString("db.username")
    val password = config.getString("db.password")
  }

  val quill = config.getConfig("quill")

  object Crawler {
    val threads = config.getInt("crawler.threads")
    val sleep = toFiniteDuration(config.getDuration("crawler.sleep"))
  }

  object HttpServer {
    val host = config.getString("httpserver.host")
    val port = config.getInt("httpserver.port")
  }

}
