package com.avantstay.headlines.http

import com.avantstay.headlines.ServerConfig
import cats.effect.{ConcurrentEffect, ContextShift, Timer}
import fs2.Stream
import org.http4s.implicits._
import org.http4s.server.blaze.BlazeServerBuilder
import org.http4s.server.middleware.Logger

import scala.concurrent.ExecutionContext

object HeadlinesServer {

  def stream[F[_] : ConcurrentEffect](implicit T: Timer[F], C: ContextShift[F], ec: ExecutionContext): Stream[F, Nothing] = {
    val httpApp = HeadlinesRoutes.route[F].orNotFound
    val finalHttpApp = Logger.httpApp(true, true)(httpApp)
    for {
      exitCode <- BlazeServerBuilder[F](ec)
        .bindHttp(ServerConfig.HttpServer.port, ServerConfig.HttpServer.host)
        .withHttpApp(finalHttpApp)
        .serve
    } yield exitCode
  }.drain
}
