package com.avantstay.headlines.http

import cats.effect._
import com.avantstay.headlines.db.ReadHeadlineDao
import com.avantstay.headlines.graphql.{NewsProvider, RouteHandler}
import org.http4s.HttpRoutes

import scala.concurrent.ExecutionContext

object HeadlinesRoutes {

  def route[F[_] : Sync](implicit F: ConcurrentEffect[F], cs: ContextShift[F], ec: ExecutionContext): HttpRoutes[F] =
    new RouteHandler[F](new NewsProvider(new ReadHeadlineDao)).route

}
