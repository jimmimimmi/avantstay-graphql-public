package com.avantstay.headlines.crawling

import cats.effect.Sync
import com.avantstay.headlines.db.Headline
import net.ruippeixotog.scalascraper.browser.JsoupBrowser
import net.ruippeixotog.scalascraper.dsl.DSL.Extract._
import net.ruippeixotog.scalascraper.dsl.DSL._

object NyTimesNewsProvider {

  private val hostPrefix = "https://www.nytimes.com"

  private def parse: Seq[Headline] = {
    val browser = JsoupBrowser()

    val doc = browser.get(s"$hostPrefix/")
    val anchors = (doc >?> elementList("a")).getOrElse(Nil)
    anchors.flatMap {
      a =>
        val href = a.attr("href")
        val spanText = a >?> text("div > h2 > span")
        val title = spanText match {
          case Some(t) => Some(t)
          case None => a >?> text("div > h2")
        }
        val fullLink = if (href.startsWith("/")) s"$hostPrefix$href" else href
        title.map(t => Headline(fullLink, t))
    }.filter(_.link.startsWith(s"$hostPrefix/2020"))
  }

  def getHeadlines[F[_]](implicit F: Sync[F]): F[Seq[Headline]] =
    F.delay(parse)
}
