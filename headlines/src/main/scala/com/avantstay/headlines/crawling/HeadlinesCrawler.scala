package com.avantstay.headlines.crawling


import java.util.concurrent.Executors

import cats.effect.{ConcurrentEffect, ContextShift, Timer}
import cats.syntax.all._
import com.avantstay.headlines.ServerConfig
import com.avantstay.headlines.db.WriteHeadlineDao
import fs2.Stream
import org.log4s.{Logger, getLogger}

import scala.concurrent.ExecutionContext

object HeadlinesCrawler {
  private val logger: Logger = getLogger

  implicit val ec = scala.concurrent.ExecutionContext.global

  def stream[F[_] : ConcurrentEffect](implicit T: Timer[F], cs: ContextShift[F]): Stream[F, Unit] = {
    val computationEC = ExecutionContext.fromExecutorService(Executors.newFixedThreadPool(ServerConfig.Crawler.threads))
    val sleepTime = ServerConfig.Crawler.sleep
    val writeHeadlineDao = new WriteHeadlineDao

    Stream.repeatEval(
      (for {
        headlines <- cs.evalOn(computationEC)(NyTimesNewsProvider.getHeadlines)
        _ <- writeHeadlineDao.store(headlines)
      } yield {
        logger.info(s"Successfully loaded ${headlines.size} headlines from https://nytimes.com")
      })
        .handleError(er => logger.error(s"error while crawling: $er"))
    ).metered(sleepTime)

  }

}
