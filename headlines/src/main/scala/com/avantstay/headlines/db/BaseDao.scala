package com.avantstay.headlines.db

import com.avantstay.headlines.ServerConfig
import io.getquill.{PostgresAsyncContext, SnakeCase}

abstract class BaseDao {
  implicit val ctx = new PostgresAsyncContext(SnakeCase, ServerConfig.quill)
}
