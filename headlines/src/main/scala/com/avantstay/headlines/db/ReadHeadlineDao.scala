package com.avantstay.headlines.db

import scala.concurrent.{ExecutionContext, Future}

class ReadHeadlineDao extends BaseDao {

  import ctx._

  def all(implicit ex: ExecutionContext): Future[List[Headline]] = {
    val allHeadlines = quote {
      querySchema[Headline]("headlines")
    }

    ctx.run(allHeadlines)
  }
}