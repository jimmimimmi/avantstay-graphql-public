package com.avantstay.headlines.db

import cats.effect.{ConcurrentEffect, ContextShift}

import scala.concurrent.ExecutionContext

class WriteHeadlineDao extends BaseDao {

  import ctx._

  def store[F[_]](newHeadlines: Seq[Headline])(implicit F: ConcurrentEffect[F],
                                               ec: ExecutionContext,
                                               cs: ContextShift[F]): F[Unit] = {
    val headlines = quote {
      querySchema[Headline]("headlines")
    }

    val batchUpsert = quote {
      liftQuery(newHeadlines.toList).foreach { h =>
        headlines.insert(h).onConflictUpdate(_.link)((t, e) => t.title -> e.title)
      }
    }
    implicit val ioContextShift = cats.effect.IO.contextShift(ec)

    F.liftIO(cats.effect.IO.fromFuture(cats.effect.IO(ctx.run(batchUpsert).map(_ => {}))))
  }
}