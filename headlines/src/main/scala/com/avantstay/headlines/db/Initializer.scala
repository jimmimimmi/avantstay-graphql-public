package com.avantstay.headlines.db

import com.avantstay.headlines.ServerConfig
import org.flywaydb.core.Flyway

object Initializer {

  def init: Unit = {
    val config = ServerConfig.Flyway
    migrate(
      config.schema,
      config.url,
      config.username,
      config.password)
  }

  private def migrate(schema: String, url: String, user: String, password: String): Unit =
    Flyway
      .configure()
      .dataSource(url, user, password)
      .schemas(schema)
      .load()
      .migrate()
}
