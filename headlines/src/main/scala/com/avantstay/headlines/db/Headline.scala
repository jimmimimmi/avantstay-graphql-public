package com.avantstay.headlines.db

case class Headline(link: String, title: String)
