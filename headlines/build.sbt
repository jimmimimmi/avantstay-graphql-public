val Http4sVersion = "0.20.23"
val CirceVersion = "0.13.0"
val Specs2Version = "4.1.0"
val LogbackVersion = "1.2.3"
val QuillVersion = "3.5.1"
val SangriaVersion = "2.0.0"
val SangriaSlowLogVersion = "2.0.0-M1"
val SangriaCirceVersion = "1.3.0"
val PostgresVersion = "42.2.13"
val FlywayVersion = "6.4.3"
val ScalaScraperVersion = "2.2.0"

lazy val root = (project in file("."))
  .settings(
    organization := "com.avantstay",
    name := "headlines",
    version := "0.0.1-SNAPSHOT",
    scalaVersion := "2.12.11",
    libraryDependencies ++= Seq(
      "org.http4s" %% "http4s-blaze-server" % Http4sVersion,
      "org.http4s" %% "http4s-blaze-client" % Http4sVersion,
      "org.http4s" %% "http4s-circe" % Http4sVersion,
      "org.http4s" %% "http4s-dsl" % Http4sVersion,

      "io.circe" %% "circe-generic" % CirceVersion,
      "io.circe" %% "circe-core" % CirceVersion,
      "io.circe" %% "circe-parser" % CirceVersion,
      "io.circe" %% "circe-optics" % CirceVersion,

      "org.sangria-graphql" %% "sangria" % SangriaVersion,
      "org.sangria-graphql" %% "sangria-slowlog" % SangriaSlowLogVersion,
      "org.sangria-graphql" %% "sangria-circe" % SangriaCirceVersion,

      "ch.qos.logback" % "logback-classic" % LogbackVersion,
      "org.postgresql" % "postgresql" % PostgresVersion,
      "org.flywaydb" % "flyway-core" % FlywayVersion,
      "io.getquill" %% "quill-jdbc" % QuillVersion,
      "io.getquill" %% "quill-async" % QuillVersion,
      "io.getquill" %% "quill-jdbc-monix" % QuillVersion,
      "io.getquill" %% "quill-async-postgres" % QuillVersion,
      "net.ruippeixotog" %% "scala-scraper" % ScalaScraperVersion,

      "org.specs2" %% "specs2-core" % Specs2Version % "test"
    ),
    addCompilerPlugin("org.typelevel" %% "kind-projector" % "0.10.3"),
    addCompilerPlugin("com.olegpy" %% "better-monadic-for" % "0.3.0")
  )

scalacOptions ++= Seq(
  "-deprecation",
  "-encoding", "UTF-8",
  "-language:higherKinds",
  "-language:postfixOps",
  "-feature",
  "-Ypartial-unification",
  "-Xfatal-warnings",
)
