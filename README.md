# Alexander Gridnev. Avantstay Graphql news api.

## Postgres prerequisites

Application uses PostgresDB as a persistence level. 
By default it expects running postgres instance on `localhost:5432`.
All configurable properties can be found in `application.conf`

One of the easiest ways to launch postgres is to run docker image:
```
docker run -p 5432:5432 --name avantstay -e POSTGRES_PASSWORD=avantstay -e POSTGRES_USER=avantstay -e POSTGRES_DB=avantstay -d postgres
```

## Application launch


```bash
sbt run 
```
Http server is hosted on `http://localhost:8080` by default. However it is possible to be changed by modifying `application.conf`


## GraphQL news API

### schema
```
type News {
  title: String,
  link: String,
}

type Query {
  news: [News!]!
}
```

### API uses JSON and can be reached via HTTP POST request:
```
curl -X POST localhost:8080/graphql  -H "Content-Type:application/json" -d '{"query": "{news {link, title}}"}'
```